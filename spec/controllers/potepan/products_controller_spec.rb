require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }

  describe 'GET #show' do
    it 'assigns the requested product to @product' do
      get :show, params: { id: product.id }
      expect(assigns(:product)).to eq product
    end

    it 'renders the :show template' do
      get :show, params: { id: product.id }
      expect(response).to render_template :show
    end
  end
end
