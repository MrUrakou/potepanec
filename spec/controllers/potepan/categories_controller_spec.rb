require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do

  describe 'GET #show' do

      before do
        @taxonomy_brand = create(:taxonomy)
        @taxonomy_category = create(:taxonomy, name: "Categories")
      end

      let(:ruby) { create(:taxon, name: "ruby", taxonomy: @taxonomy_brand) }
      let(:python) { create(:taxon, name: "python", taxonomy: @taxonomy_brand) }

      let(:ruby_products) do
        %w(ruby-mag ruby-shirt ruby-bag).map do |product_name|
          create(:product, name: product_name) do |product|
            product.taxons << ruby
          end
        end
      end

      let(:python_products) do
        %w(python-mag python-shirt python-bag).map do |product_name|
          create(:product, name: product_name) do |product|
            product.taxons << python
          end
        end
      end

    context "params(:id)に依存しないインスタンス変数の確認" do

      let(:brand_taxon) { Spree::Taxon.roots.find_by( name: "Brand") }
      let(:category_taxon) { Spree::Taxon.roots.find_by( name: "Categories") }

      it "任意のtaxonに@brandが割り当てられるか確認" do
        get :show, params: { id: ruby.id }
        expect(assigns(:brand)).to eq brand_taxon
      end

      it "任意のtaxonに@categoryが割り当てられるか確認" do
        get :show, params: { id: ruby.id }
        expect(assigns(:category)).to eq category_taxon
      end
    end

    context "params(:id)によって割り当てられるインスタンス変数の確認" do

      it "taxonに紐付いた商品が@productsに割り当てられる" do
        get :show, params: { id: ruby.id }
        expect(assigns(:products)).to match ruby_products
      end

      it "taxonに紐付いていない違うカテゴリーの商品は@productsに割り当てられない" do
        get :show, params: { id: ruby.id }
        expect(assigns(:products)).to_not include python_products[0]
      end
    end


    context "gridボタン、listボタンを押して表示方法が切り替わるかの確認" do

      it 'renders the :show template by product_grid style' do
        get :show, params: { id: ruby.id, view: 'product_grid' }
        expect(response).to render_template :show
      end

      it 'renders the :show template by product_list style' do
        get :show, params: { id: ruby.id, view: 'product_list' }
        expect(response).to render_template :show
      end
    end
  end
end
