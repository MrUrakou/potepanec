class Potepan::CategoriesController < ApplicationController
  before_action :load_partial, only: :show

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products

    @taxonomies = Spree::Taxon.roots
    @category = @taxonomies.find_by(name: "Categories")

    @brand = @taxonomies.find_by(name: "Brand")

  end

  private
    def load_partial
      @partial = params[:view] || "product_grid"
    end
end
